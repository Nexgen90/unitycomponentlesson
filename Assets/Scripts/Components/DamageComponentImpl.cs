using UnityEngine;

public class DamageComponentImpl : MonoBehaviour, DamageComponent
{
    [SerializeField]
    private float startBuff;
    [SerializeField]
    private FloatVariable attack;

    public float DealDamage()
    {
        return attack.Value + startBuff;
    }
}
