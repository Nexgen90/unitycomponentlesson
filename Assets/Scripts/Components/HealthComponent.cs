public interface HealthComponent
{
    void TakeDamage(float value);
    float GetHealth();
}