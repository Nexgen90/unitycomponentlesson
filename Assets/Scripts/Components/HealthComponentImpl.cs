using UnityEngine;

public class HealthComponentImpl : MonoBehaviour, HealthComponent
{
    [SerializeField]
    private FloatVariable targetHp;

    public void TakeDamage(float value)
    {
        targetHp.Value -= value;
    }

    public float GetHealth()
    {
        return targetHp.Value;
    }
}
