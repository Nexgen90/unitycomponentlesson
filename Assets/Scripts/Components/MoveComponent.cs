using UnityEngine;

public interface MoveComponent
{
    void Move(Vector3 direction);
    float GetSpeed();

    Vector3 GetPosition();
}
