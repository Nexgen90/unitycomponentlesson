using UnityEngine;

public class MoveComponentImpl : MonoBehaviour, MoveComponent
{
    [SerializeField]
    private Transform targetTransform;
    [SerializeField]
    private FloatVariable speed;
    
    public void Move(Vector3 direction)
    {
        targetTransform.position += direction * (Time.deltaTime * speed.Value);
    }

    public float GetSpeed()
    {
        return speed.Value;
    }

    public Vector3 GetPosition()
    {
        return targetTransform.position;
    }
}