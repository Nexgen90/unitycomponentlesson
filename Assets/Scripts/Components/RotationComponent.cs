using UnityEngine;

public interface RotationComponent
{
    Quaternion GetRotation();
}