using UnityEngine;

public class RotationComponentImpl : MonoBehaviour, RotationComponent
{
    [SerializeField]
    private Quaternion rotation;
    
    public Quaternion GetRotation()
    {
        return rotation;
    }
}
