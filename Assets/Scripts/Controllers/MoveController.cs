using System;
using Entities;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    [SerializeField]
    private MonoEntity target;
    private MoveComponent moveComponent;

    private void Start()
    {
        moveComponent = target.Element<MoveComponent>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            moveComponent.Move(new Vector3(0, 0, 1.0f));
        }
        
        if (Input.GetKey(KeyCode.DownArrow))
        {
            moveComponent.Move(new Vector3(0, 0, -1.0f));
        }
        
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            moveComponent.Move(Vector3.left);
        }
        
        if (Input.GetKey(KeyCode.RightArrow))
        {
            moveComponent.Move(Vector3.right);
        }
    }
}
