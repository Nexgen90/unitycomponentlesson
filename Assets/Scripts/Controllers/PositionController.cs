using System;
using Entities;
using UnityEngine;

public class PositionController : MonoBehaviour
{
    [SerializeField]
    private MonoEntity target;

    private MoveComponent moveComponent;
    private RotationComponent rotationComponent;

    private void Start()
    {
        moveComponent = target.Element<MoveComponent>();
        rotationComponent = target.Element<RotationComponent>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            printPosition();
        }
    }

    private void printPosition()
    {
        Vector3 position = moveComponent.GetPosition();
        Quaternion rotation = rotationComponent.GetRotation();
        
        Debug.Log($"Player position: {position}, rotation: {rotation}");
    }
}
