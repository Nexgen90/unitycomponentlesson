using Entities;
using UnityEngine;

public class SelfDamageController : MonoBehaviour
{
    private HealthComponent healthComponent;
    private DamageComponent damageComponent;
    
    [SerializeField]
    private MonoEntity target;

    private void Start()
    {
        healthComponent = target.Element<HealthComponent>();
        damageComponent = target.Element<DamageComponent>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            healthComponent.TakeDamage(damageComponent.DealDamage());
        }
    }
}
