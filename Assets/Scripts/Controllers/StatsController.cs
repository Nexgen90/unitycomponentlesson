using Entities;
using UnityEngine;

public class StatsController : MonoBehaviour
{
    [SerializeField]
    private MonoEntity target;

    private MoveComponent moveComponent;
    private DamageComponent damageComponent;
    private HealthComponent healthComponent;


    private void Start()
    {
        moveComponent = target.Element<MoveComponent>();
        damageComponent = target.Element<DamageComponent>();
        healthComponent = target.Element<HealthComponent>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            printStats();
        }
    }

    private void printStats()
    {
        Debug.Log($"Player stats: " +
                  $"Speed: {moveComponent.GetSpeed()}, " +
                  $"Attack: {damageComponent.DealDamage()}, " +
                  $"Health: {healthComponent.GetHealth()}");
    }
}